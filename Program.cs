﻿using System;
using System.Collections.Generic;
using System.Linq;
using Combinatorics.Collections;

namespace QualityCalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            string input = string.Empty;
            while (true)
            {
                Console.WriteLine("Enter item qualities (separated by spaces) or exit");
                input = Console.ReadLine();
                if (input == "exit") return;

                int[] numbers = null;
                try
                {
                    numbers = input.Split(' ', StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToArray();

                    if (numbers.Length < 2)
                    {
                        Console.WriteLine("You need at least two items");
                        continue;
                    }
                }
                catch
                {
                    Console.WriteLine("Error in input, try again.");
                    continue;
                }

                var found = new List<IEnumerable<int>>();
                for (int i = 2; i <= numbers.Length; i++)
                {
                    var combinations = new Combinations<int>(numbers, i);
                    found.AddRange(combinations.Where(c => c.Sum() == 40));
                }

                if (!found.Any())
                    Console.WriteLine("Did not found any reasonable combinations");
                else
                {
                    Console.WriteLine("Found the following combinations:");
                    Console.WriteLine(string.Join(Environment.NewLine, found.Select(f => string.Join(' ', f.OrderBy(x => x))).Distinct()));
                }
            }
        }
    }
}
